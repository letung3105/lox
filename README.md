Interpreter implementations for the Lox language. Lox is a very basic programming language created for the [Crafting Interpreters] book, written by [Bob Nystrom]. Visit [chapter 3] for an overview of the language. Also visit the [author's Github repository] to see the original implementation and many other implementations in different languages.

Vist [glox](/glox) for the implementation of the first interpreter described in the book.

Vist [rlox](/rlox) for the implementation of the second interpreter described in the book.

[author's Github repository]: https://github.com/munificent/craftinginterpreters
[Bob Nystrom]: https://github.com/munificent
[chapter 3]: http://craftinginterpreters.com/the-lox-language.html
[Crafting Interpreters]: http://craftinginterpreters.com/
